package BaseTests;

import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 25.04.18
 * Time: 14:24
 * To change this template use File | Settings | File Templates.
 */
public class TestsMirRybalki {
    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        Configuration.browser = "chrome";
        open("https://mir-rybalki.com/" );
    }
        @Test
        public void userComMirrybalki(){
            $(By.xpath("/html/body/div[5]/div[1]/div[2]/ul/li[1]/div/ul/li[5]/a"));
            $(By.linkText("Складная мебель для рыбалки и туризма")).click();
            $(By.id("link_to_product_294347153")).click();
            $(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/div[1]/div/p/span[1]")).shouldHave(text("135"));
            System.out.println("135");
            $(By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[2]/a")).click();
            $(By.className("x-shc-total__price")).shouldHave(text("135 грн"));
            System.out.println("135 грн. - ПОКУПАЙ!!!");
            $(By.className("x-shc-item__control-icon")).click();
            $(By.xpath("/html/body/div[17]/div/div/div/div/div[1]/div")).shouldHave(text("В корзине нет товаров"));
            System.out.println("В корзине нет товаров...ПЕРЕДУМАЛ!!!");



 }
}
